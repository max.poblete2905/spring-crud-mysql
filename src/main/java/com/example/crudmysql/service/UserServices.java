package com.example.crudmysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.crudmysql.dao.IUserDao;
import com.example.crudmysql.model.UserEntity;

import lombok.Data;

@Service
public class UserServices implements IUserServices {

	@Autowired
	private IUserDao userDao;

	@Override
	public List<UserEntity> search() {
		return (List<UserEntity>) userDao.findAll();
	}

	@Override
	public UserEntity searchById(long userId) {
		java.util.Optional<UserEntity> userOptional = this.userDao.findById(userId);
		return userOptional.orElse(null);
	}

	@Override
	public UserEntity addUser(UserEntity userEntity) {
		java.util.Optional<UserEntity> existingUser = this.userDao.findById(userEntity.getUserId());
		if (existingUser.isPresent()) {
			System.out.print("Ya existe usuario");
		} else {
			return this.userDao.save(userEntity);
		}
		return userEntity;
	}

	@Override
	public UserEntity updateUser(UserEntity userEntity) {
		return this.userDao.save(userEntity);
	}

	@Override
	public boolean deleteUser(long userId) {
		 this.userDao.deleteById(userId);
		return true;
	}

}
