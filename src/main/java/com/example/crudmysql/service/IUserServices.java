package com.example.crudmysql.service;

import java.util.List;

import com.example.crudmysql.model.UserEntity;

public interface IUserServices {

	public List<UserEntity> search();

	public UserEntity searchById(long userId);

	public boolean deleteUser(long userId);

	public UserEntity addUser(UserEntity userEntity);

	public UserEntity updateUser(UserEntity userEntity);

}
