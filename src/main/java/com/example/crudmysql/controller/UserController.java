package com.example.crudmysql.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.crudmysql.model.UserEntity;
import com.example.crudmysql.service.IUserServices;

@RestController
public class UserController {

	@Autowired
	private IUserServices service;

	@GetMapping("users")
	public List<UserEntity> search() {
		return this.service.search();
	}

	@GetMapping("user/{userId}")
	public UserEntity searchById(
			@PathVariable("userId") long userId, @RequestParam("state") boolean state) {
		System.out.print("STATE::::" + state);
		return this.service.searchById(userId);
	}

	@PutMapping("user")
	public UserEntity userUpdate(@RequestBody UserEntity userEntity) {
		return this.service.updateUser(userEntity);
	}

	@DeleteMapping("user/{userId}")
	public boolean deleteUser(@PathVariable("userId") long userId) {
		return this.service.deleteUser(userId);
	}

	@PostMapping("/user")
	public UserEntity addUser(@RequestBody UserEntity userEntity) {
		return this.service.addUser(userEntity);
	}
}
