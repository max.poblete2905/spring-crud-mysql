package com.example.crudmysql.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="user_entity")
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long userId;

	@Column(name = "user_name")
	private String userNane;

	@Column(name = "user_rut")
	private String userRut;

	@Column(name = "user_address")
	private String userAddress;
	
	@Column(name = "user_state")
	private String userState;

	public long getUserId() {
		return userId;
	}

	public String getUserRut() {
		return userRut;
	}

	public void setUserRut(String userRut) {
		this.userRut = userRut;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserNane() {
		return userNane;
	}

	public void setUserNane(String userNane) {
		this.userNane = userNane;
	}

	public String getUserAddres() {
		return userAddress;
	}

	public void setUserAddres(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}
}
