package com.example.crudmysql.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.crudmysql.model.UserEntity;

public interface IUserDao extends JpaRepository<UserEntity, Long> {

}
